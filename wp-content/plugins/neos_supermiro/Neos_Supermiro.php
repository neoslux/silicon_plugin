<?php
/*
Plugin Name: neos_supermiro plugin
Description: plugin that manages the import of articles from supermiro website
Version: 0.1
Author: Neos
Author URI: https://neos.lu/
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
wp_enqueue_style( 'stylePlugin', plugins_url( 'style.css', __FILE__ ) . '?v=' . time() );
class Neos_Supermiro {

	// create the wp database for neos_supermiro plugin
	static function create_db() {

		global $wpdb ;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = $wpdb->get_charset_collate();

		// create neos_supermiro_events and neos_supermiro_logs table if don't exist

		// check if tables exist
		$table_name1 = $wpdb->prefix . 'neos_supermiro_events' ;
		$table_name2 = $wpdb->prefix . 'neos_supermiro_logs' ;
		$query1 = $wpdb->prepare( "SHOW TABLES LIKE %s", $wpdb->esc_like( $table_name1 ) );
		$query2 = $wpdb->prepare( "SHOW TABLES LIKE %s", $wpdb->esc_like( $table_name2 ) );
		$table1Exists = ( $wpdb->get_var( $query1 ) == $table_name1 ) ;
		$table2Exists = ( $wpdb->get_var( $query2 ) == $table_name2 ) ;
		if ( $table1Exists && $table2Exists ) {
			return false;
		}

		// create neos_supermiro_events table
		$sql1 = "CREATE TABLE $table_name1 (
            id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
            id_supermiro MEDIUMINT(9) NOT NULL,
            title VARCHAR(64) DEFAULT 'Title' NOT NULL,
            description TEXT NOT NULL,
            place_name VARCHAR(32) DEFAULT 'Place' NOT NULL,
            city VARCHAR(32) DEFAULT 'City' NOT NULL,
            start_date DATE DEFAULT '2009-04-01' NOT NULL,
            end_date DATE DEFAULT '2014-04-20' NOT NULL,
            start_time TIME DEFAULT '00:00:00' NOT NULL,
            event_url VARCHAR(255) DEFAULT 'Event url' NOT NULL,
            picture_url VARCHAR(255) DEFAULT 'Picture url' NOT NULL,
            picture LONGTEXT NOT NULL,
            PRIMARY KEY  (id)
            ) $charset_collate;";
		dbDelta( $sql1 );

		// create neos_supermiro_logs table
		$sql2 = "CREATE TABLE $table_name2 (
	        id mediumint(9) NOT NULL AUTO_INCREMENT,
	        update_date TIMESTAMP NOT NULL,
	        updated_rows VARCHAR(3),
	        PRIMARY KEY  (id)
	        ) $charset_collate;";
		dbDelta( $sql2 );

		return true ;
	}

	// update the neos_supermiro plugin database with supermiro contents
	static function update_db() {

		global $wpdb ;

		// check if the wpdb has been updated during the past 24 hours
		$logsTable = $wpdb->prefix . 'neos_supermiro_logs' ;
		$lastUpdate = $wpdb->get_var("SELECT MAX(update_date) FROM $logsTable") ;
		$update = ( time() - strtotime($lastUpdate) > (60*60*24) ) ;
		if ( !$update ) {
			return false ;
		}

		// get the json from supermiro
		$supermiro            = file_get_contents( "https://www.supermiro.com/api/1.0/fetch/jgfb84r1ra" );
		$supermiroEventsArray = json_decode( $supermiro );

		// insert the event in the wp database if it doesn't exist
		$eventsTable = $wpdb->prefix . 'neos_supermiro_events' ;
		$insertedEvents = 0 ;
		foreach ( $supermiroEventsArray as $key => $event ) {
			//check whether the event is already in the wpdb
			$id          = $event->id ;
			$eventExists = $wpdb->get_var( "SELECT COUNT(*) FROM $eventsTable WHERE id_supermiro = '" . $id . "' LIMIT 1" );
			// if not, insert in the wpdb
			if ( !$eventExists > 0 ) {
				$startDate    = DateTime::createFromFormat( 'd-m-Y', $event->startDate );
				$startDate    = $startDate->format( 'Y-m-d' );
				$endDate    = DateTime::createFromFormat( 'd-m-Y', $event->endDate );
				$endDate    = $endDate->format( 'Y-m-d' );
				$wpdb->insert( $eventsTable, array(
					'id_supermiro'=> $id,
					'title'       => $event->title,
					'description' => $event->description,
					'place_name'  => $event->placeName,
					'city'        => $event->city,
					'start_date'  => $startDate,
					'end_date'    => $endDate,
					'start_time'  => $event->startTime,
					'event_url'   => $event->eventUrl,
					'picture_url' => $event->pictureUrl,
					'picture'     => $event->picture
				) ) ;
				$insertedEvents += 1 ;
			} //endif
		} //endforeach

		// update the log table

		$wpdb->insert( $logsTable, array(
			'update_date' => date('Y-m-d H:i:s'),
			'updated_rows' => $insertedEvents
		)) ;

		return true ;
	}

	// display the events
	static function display_events($atts) {

		global $wpdb ;

		// get the attributes
		$atts = shortcode_atts( array(
			'eventsnumber' => 6,
			'displaytype' => 'list'
		), $atts, 'neos_supermiro' );
		$eventsNumber = $atts['eventsnumber'] ;
		$displayType = $atts['displaytype'] ;

		// Refresh the db if needed
		self::update_db() ;

		// Get the last events
		$date = date('Y-m-d') ;
		$eventsTable = $wpdb->prefix . 'neos_supermiro_events' ;
		$eventsArray = $wpdb->get_results("SELECT * FROM {$eventsTable} WHERE DATEDIFF(start_date,'{$date}')>=0 ORDER BY id_supermiro DESC LIMIT {$eventsNumber}", ARRAY_A) ;

		// Display list
		if ( $displayType == 'list') {
			$html = '<ul class="neos-supermiro-list">' ;
			foreach ($eventsArray as $key=>$event) {
				// modify the date format
				$startDate    = DateTime::createFromFormat( 'Y-m-d', $event['start_date'] );
				$startDate    = $startDate->format( 'd-m-Y' );
				// create the html
				$html .= '<li>' ;
				$html .= '<h3>' . $event['title'] . '</h3>' ;
				$html .= '<span>' . $startDate . ' - ' . substr($event['start_time'],0,5). '</span><br>' ;
				$html .= '<span>' . $event['place_name'] . ' - ' . $event['city'] . '</span><br>' ;
				$html .= '<image src="' . $event['picture_url'] . '">' ;
				$html .= '<p>' . $event['description'] . '</p>' ;
				$html .= '</li>' ;
			}
			$html .= '</ul>' ;

			return $html ;
		}

		// Display article
		if ( $displayType == 'article') {
			$html = '<article class="neos-supermiro-article">' ;
			foreach ($eventsArray as $key=>$event) {
				// modify the date format
				$startDate    = DateTime::createFromFormat( 'Y-m-d', $event['start_date'] );
				$startDate    = $startDate->format( 'd-m-Y' );
				// create the html
				$html .= '<h3>' . $event['title'] . '</h3>' ;
				$html .= '<span>' . $startDate . ' - ' . substr($event['start_time'],0,5) . '</span><br>' ;
				$html .= '<span>' . $event['place_name'] . ' - ' . $event['city'] . '</span><br>' ;
				$html .= '<image src="' . $event['picture_url'] . '">' ;
				$html .= '<p>' . $event['description'] . '</p>' ;
			}
			$html .= '</article>' ;

			return $html ;
		}

		// Display carousel
		if ($displayType == 'carousel') {
			$html = '<div class="neos-supermiro-events">';
			$html .= '<div class="neos-supermiro-logo">';
			$html .= '<h2 class="neos-supermiro-titre-events-agenda">THE EVENTS AGENDA by </h2>';
			$html .= '<img class="neos-supermiro-titre-header-img" src="' . plugins_url() . '/neos_supermiro/img/logo-supermiro.png" />';
			$html .= '</div>';
			$html .= '<div class="neos-supermiro-carousel">';

			foreach ($eventsArray as $key=>$event) {
				// modify the date format
				$startDate    = DateTime::createFromFormat( 'Y-m-d', $event['start_date'] );
				$startDate    = $startDate->format( 'l, jS M' );
				// create the html

				$html .= '<div class="neos-supermiro-event">';
				$html .= '<div class="neos-supermiro-container-for-flex">';
				$html .= '<div class="neos-supermiro-image">';
				$html .= '<a  href="' . $event['event_url'] . '" target="_blank">';
				$html .= '<image class="neos-supermiro-img" src="' . $event['picture_url'] . '">' ;
				$html .= '</a>';
				$html .= '</div>'; // neos-supermiro-image
				$html .= '<div class="neos-supermiro-body">';
				$html .= '<a href="' . $event['event_url'] . '" target="_blank">';
				$html .= '<span class="date-after-photo">' . $startDate . ' - ' . substr($event['start_time'],0,5) . '</span>';
				$html .= '<div class="title">' . $event['place_name'] . ' - ' . $event['city'] .  '</div>' ;
				$html .= '<div class="subtitle">' . $event['title'] . '</div>';
				$html .= '<div class="neos-descrption-ellipsis"><p class="description">' . substr($event['description'],0,100 ). '...</p></div>';


				$html .= '<div class="read-more"><span class="read-more-info">+ INFO</span></div>' ;
				$html .= '</a>';
				$html .= '</div>' ; // neos-supermiro-body
				$html .= '</div>' ; // neos-supermiro-container-for-flex

				$html .= '</div>' ; // neos-supermiro-event

			}
			$html .= '</div>' ; // neos-supermiro-carousel
			$html .= '<div class="neos-supermiro-logo-footer" >';
			$html .= '<span  class="neos-supermiro-footer-titre" target="_blank" href="https://www.supermiro.com/fr/luxembourg">voir tout l\'agenda</span>';
			$html .= '</div>';// neos-supermiro-footer
			$html .= '</div>'; // neos-supermiro-events

			return $html ;
		}

	}

	// drop the tables (at the deactivation)
	static function drop_db() {
		global $wpdb;
		$table_name1 = $wpdb->prefix . 'neos_supermiro_events' ;
		$table_name2 = $wpdb->prefix . 'neos_supermiro_logs' ;
		$sql1 = "DROP TABLE IF EXISTS $table_name1";
		$sql2 = "DROP TABLE IF EXISTS $table_name2";
		$wpdb->query($sql1);
		$wpdb->query($sql2);
	}
} // end of class

// activation & deactivation hooks
register_activation_hook( __FILE__, array( 'Neos_Supermiro', 'create_db' ) ) ;
register_deactivation_hook(__FILE__, array( 'Neos_Supermiro', 'drop_db' )) ;

// creation of the menu in the admin panel
add_action( 'admin_menu', 'neos_supermiro_menu' );
function neos_supermiro_menu() {
	if (!current_user_can('manage_options'))
	{
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
	add_menu_page( 'Neos Supermiro plugin', 'neos_supermiro', 'manage_options', 'neos_supermiro', 'neos_menu');
}
function neos_menu() {
	$html = '<h1>Neos_Supermiro plugin</h1>' ;
	$html .= '<p>Here are few tips for this plugin...</p>' ;
	$html .= '<h2>How to create the shortcode</h2>' ;
	$html .= '<h3>Example</h3>' ;
	$html .= '<p>Here is an example of the shortcode for this plugin : [neos_supermiro eventsnumber=3 displaytype="article"]</p>';
	$html .= '<h3>The attributes</h3>' ;
	$html .= '<p>This shortcode accepts 2 attributes : </p>' ;
	$html .= '<ul><li>eventsnumber : define the number of events to be displayed (integer)</li>' ;
	$html .= '<li>displaytype : define how the events will be displayed. "list", "article" or "carousel" (string)</li></ul>' ;
	$html .= '<p>You can however omit one or both attributes. Default values are resp. 6 and "list"</p><br>' ;
	$html .= '<p>Enjoy !</p>' ;

	echo $html ;
}

// Add css and scripts for slick carousel
add_action('wp_enqueue_scripts','neos_supermiro_init');

function neos_supermiro_init() {
	wp_enqueue_style('slickcss', plugins_url('/slick/slick.css', __FILE__));
	wp_enqueue_style('slickthemecss', plugins_url('/slick/slick-theme.css', __FILE__));
	wp_enqueue_script(('slick'), plugins_url('/slick/slick.min.js', __FILE__), array('jquery'));
	wp_enqueue_script( 'neos_supermiro_script', plugins_url( '/js/script.js', __FILE__ ), array('jquery'));
}


// update of the db hook
add_action('init',array('Neos_Supermiro', 'update_db')) ;

// creation of the shortcode
add_shortcode( 'neos_supermiro', array( 'Neos_Supermiro', 'display_events' ) );
