jQuery(function($){
    if ($(window).width() <= 768){
        $('.neos-supermiro-carousel').slick({
            dots: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            speed: 1000,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                        rows: 2,
                        slidesPerRow:2,
                        mobileFirst:true
                    }
                }

                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }else{
        $('.neos-supermiro-carousel').slick({
            dots: true,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            speed: 1000,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                        rows: 1,
                        slidesPerRow:1,
                        mobileFirst:false,
                        arrows: true
                    }
                }

                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }
    // container as a link - header
    $('.neos-supermiro-logo').on('click',function(){
        window.open('https://www.supermiro.com/fr/luxembourg','_blank');
    });
    // container as a link - footer
    $('.neos-supermiro-logo-footer').on('click',function(){
        console.log('click');
        window.open('https://www.supermiro.com/fr/luxembourg','_blank');
                                                    });
});