=== Neos_Supermiro ===
Contributors: Alexandre
Donate link: https://neos.lu/
Tags: Events, Luxembourg
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 0.1
Requires PHP: 5.4
License:
License URI:

Include easily the best events to come in Luxembourg in your wp website !

== Description ==

This plugin allow you to include a list of the latest events to come in Luxembourg based on supermiro.com website information. Get them as a simple list or as articles by adding a shortcode on your site.

== Installation ==

Please copy the "neaos_supermiro" folder directly in "your-wp-folder > wp-content > plugins".
Open the wp-admin dasboard and go to the plugins page. Activate the neos_supermiro plugin.
Warning ! Deactivating this plugin will drop the associated database... you wil loose all recorded events !

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==


== Upgrade Notice ==

